/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import Presentacion.Mensajes.MensajesDialogos;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JRootPane;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

/**
 *
 * @author Eric
 */
public class BarcodeGenerator {
    public static void main(String[] args) throws IOException {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("01010138");
        lista.add("02020356XL");
        BarcodeGenerator.generarBarras( lista, null );
    }
    
    public static void generarBarras( ArrayList<String> codigosParaGenerar, JRootPane rootPane ) {
        int y = 0;
        Code128Bean bean = new Code128Bean();
        bean.setModuleWidth( 0.4 );
        bean.doQuietZone(false);
        BitmapCanvasProvider provider = new BitmapCanvasProvider( 150, BufferedImage.TYPE_BYTE_GRAY, true, 0);
        
        BufferedImage barcodeImage;
        // Imagen Grande contenedora de las demás
        BufferedImage resultado = new BufferedImage( 500, 150 * codigosParaGenerar.size(), BufferedImage.TYPE_BYTE_GRAY );
        // Obtenemos el lienzo de la imagen final y la pintamos de blanco.
        Graphics g = resultado.getGraphics();
        g.fillRect( 0, 0, resultado.getWidth(), resultado.getHeight());
        // Acá se genera cada código de barra
        for (int i = 0; i < codigosParaGenerar.size(); i++) {
            bean.generateBarcode( provider, codigosParaGenerar.get(i) );
            barcodeImage = provider.getBufferedImage();
            g.drawImage(barcodeImage, 0, y, null);
            y += 150;
        }
        
        
        // Para solicitar el path donde dejar la Imagen.
        JFileChooser j = new JFileChooser();        
        j.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
        j.showDialog( null, "Elegir Directorio");
        File archivo = j.getSelectedFile();
        try {
            //System.out.println( archivo.getPath() );
            //ImageIO.write(resultado, "gif", new File( "C:\\Users\\Eric\\Desktop" + "\\" + codigosParaGenerar.size() + " - Códigos.gif"  ) );
            ImageIO.write(resultado, "gif", new File( archivo.getPath() + "\\" + codigosParaGenerar.size() + " - Códigos.gif"  ) );
            MensajesDialogos.archivoGeneradoCorrectamente( rootPane );
        } catch (IOException ex) {
            MensajesDialogos.errorCrearCodigo( rootPane );
        }
        
    }
}