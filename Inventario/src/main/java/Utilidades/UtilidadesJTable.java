/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Eric
 */
public class UtilidadesJTable {
    
    public static void esconderColumnaTabla( JTable tabla, int columna){
        tabla.getColumnModel().getColumn(columna).setMinWidth(0);
        tabla.getColumnModel().getColumn(columna).setMaxWidth(0);
        tabla.getColumnModel().getColumn(columna).setPreferredWidth(0);
    }
    
    public static void evitarMoverColumnas( JTable tabla ){
        tabla.getTableHeader().setReorderingAllowed(false);
    }
    
    public static void removerTodasFilas ( JTable tabla ){
        DefaultTableModel dm = (DefaultTableModel) tabla.getModel();
        int rowCount = dm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            dm.removeRow(i);
        }
    }
}
