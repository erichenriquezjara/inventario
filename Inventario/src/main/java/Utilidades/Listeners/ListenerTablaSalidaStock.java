/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.Mensajes.MensajesDialogos;
import Presentacion.SalidaStock;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Eric
 */
public class ListenerTablaSalidaStock implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaSalidaStock(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        SalidaStock padre = ( (SalidaStock) this.padre);
        int fila_seleccionada = padre.getTblArticulosBusqueda().getSelectedRow();
        if ( fila_seleccionada != -1) {
            JTable tbl = padre.getTblArticulosBusqueda();
            String cantidadDisponible = tbl.getValueAt( fila_seleccionada, 3).toString();
            Integer cantidadMaxima = Integer.valueOf( cantidadDisponible );
            
            if ( cantidadMaxima < Integer.valueOf( padre.getTxtCantidad().getText() ) ) {
                MensajesDialogos.solicitarMasDeLoExistente(padre.getRootPane() );
            };
        }
    }
}