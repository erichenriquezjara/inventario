/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.MantenedorCentroCosto;
import Utilidades.ComboBoxItem;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Eric
 */
public class ListenerTablaCentroCostos implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaCentroCostos(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        MantenedorCentroCosto padre = ( (MantenedorCentroCosto) this.padre);
        padre.fila_seleccionada = padre.getTblCentroCostos().getSelectedRow();
        if ( padre.fila_seleccionada != -1) {                        
            JTable tbl = padre.getTblCentroCostos();
            padre.getTxtNombre().setText( tbl.getValueAt(padre.fila_seleccionada, 1).toString() );
            padre.getTxtGetCodigo().setText( tbl.getValueAt(padre.fila_seleccionada, 2).toString() );
            padre.getTxtComentario().setText( tbl.getValueAt(padre.fila_seleccionada, 3).toString() );
        }
    }
}
