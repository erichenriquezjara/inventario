/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.MantenedorArticulo;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Eric
 */
public class ListenerTablaArticulos implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaArticulos(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        MantenedorArticulo padre = ( (MantenedorArticulo) this.padre);
        padre.fila_seleccionada = padre.getTblArticulos().getSelectedRow();
         if ( padre.fila_seleccionada != -1) {            
            JTable tbl = padre.getTblArticulos();
            
            String codigoBorrar = "";
            codigoBorrar += padre.getTxtCodigoGrupo().getText();
            codigoBorrar += padre.getTxtCodigoSubGrupo().getText();
            codigoBorrar += padre.getTxtCodigoMarca().getText();
            
            String codigoArticulo = tbl.getValueAt(padre.fila_seleccionada, 1).toString();
            
            padre.getTxtCodigoArticulo().setText( codigoArticulo.replace( codigoBorrar, "") );
            padre.getTxtDescripcion().setText( tbl.getValueAt(padre.fila_seleccionada, 2).toString() );
        }
    }
}
