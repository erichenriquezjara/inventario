package Utilidades.Listeners;

import Presentacion.BusquedaSolicitudes;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ListenerJDayChooser
        implements PropertyChangeListener {

    BusquedaSolicitudes padre;

    public ListenerJDayChooser(BusquedaSolicitudes padre) {
        this.padre = padre;
    }

    public void propertyChange(PropertyChangeEvent evt) {
        this.padre.iniciarTablaSolicitudes("POR_FECHA");
    }
}
