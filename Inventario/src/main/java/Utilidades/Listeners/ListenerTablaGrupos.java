/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import Presentacion.MantenedorGrupo;

/**
 *
 * @author Eric
 */
public class ListenerTablaGrupos implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaGrupos(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        MantenedorGrupo padre = ( (MantenedorGrupo) this.padre);
        padre.fila_seleccionada = padre.getTblGrupos().getSelectedRow();
         if ( padre.fila_seleccionada != -1) {            
            JTable tbl = padre.getTblGrupos();
            padre.getTxtDescripcion().setText( tbl.getValueAt(padre.fila_seleccionada, 1).toString() );
            padre.getTxtCodigo().setText( tbl.getValueAt(padre.fila_seleccionada, 2).toString() );
        }
    }
}
