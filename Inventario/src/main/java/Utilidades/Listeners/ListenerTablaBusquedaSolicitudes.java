/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.BusquedaSolicitudes;
import javax.swing.JFrame;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Eric
 */
public class ListenerTablaBusquedaSolicitudes implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaBusquedaSolicitudes(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        BusquedaSolicitudes padre = ( (BusquedaSolicitudes) this.padre);
        int selectedRow = padre.getTblSolicitudes().getSelectedRow();
        
        System.out.println("Fila Seleccionada ->" + selectedRow );
        
        if ( selectedRow != -1 ) {
            padre.buscarDetalle();            
        }
    }
}
