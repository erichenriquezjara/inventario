/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.MantenedorMarca;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Eric
 */
public class ListenerTablaMarcas implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaMarcas(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        MantenedorMarca padre = ( (MantenedorMarca) this.padre);
        padre.fila_seleccionada = padre.getTblMarcas().getSelectedRow();
         if ( padre.fila_seleccionada != -1) {            
            JTable tbl = padre.getTblMarcas();
            padre.getTxtDescripcion().setText( tbl.getValueAt(padre.fila_seleccionada, 1).toString() );
            padre.getTxtCodigo().setText( tbl.getValueAt(padre.fila_seleccionada, 2).toString() );
        }
    }
}
