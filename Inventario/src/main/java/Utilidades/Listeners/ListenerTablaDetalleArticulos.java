/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.BusquedaSolicitudes;
import javax.swing.JFrame;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author USER
 */
public class ListenerTablaDetalleArticulos implements ListSelectionListener{

    private final JFrame padre;
    
    public ListenerTablaDetalleArticulos(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        BusquedaSolicitudes padre = ( (BusquedaSolicitudes) this.padre);
        padre.buscarDetalle();
    }
    
}
