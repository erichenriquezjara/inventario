/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades.Listeners;

import Presentacion.DevolucionStock;
import Presentacion.Mensajes.MensajesDialogos;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Eric
 */
public class ListenerTablaDetalleArticulosSol implements ListSelectionListener{

    private final JFrame padre;

    public ListenerTablaDetalleArticulosSol(JFrame frame){
        this.padre = frame;
    }
    
    @Override
    /* Este método está sobreescrito para que al momento de seleccionar una fila, esta se escriba en los controles */
    public void valueChanged(ListSelectionEvent e) {
        DevolucionStock padre = ( (DevolucionStock) this.padre);
        int fila_seleccionada = padre.getTblDetalleArticulosSol().getSelectedRow();
        if ( fila_seleccionada != -1) {
            JTable tbl = padre.getTblDetalleArticulosSol();
            String cantidadDisponible = tbl.getValueAt( fila_seleccionada, 5).toString();
            System.out.println("Cantidad -> " + cantidadDisponible);
            Integer cantidadMaxima = Integer.valueOf( cantidadDisponible );
            
            if ( cantidadMaxima < Integer.valueOf( padre.getTxtCantidad().getText() ) ) {
                MensajesDialogos.devolverMasDeLoSolicitado( padre.getRootPane() );
            }
        }
    }
}