/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Eric
 */
public class InventarioComboBoxModel extends DefaultComboBoxModel<ComboBoxItem> {   
    
    public InventarioComboBoxModel( ComboBoxItem[] datos ){
        super( datos );
    }
    
    @Override
    public ComboBoxItem getSelectedItem(){
        ComboBoxItem retorno = (ComboBoxItem) super.getSelectedItem();
        return retorno;
    }
}
