/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import Modelo.ClasesJPA.Grupo;
import Modelo.ClasesJPA.Marca;
import Modelo.ClasesJPA.CentroCostos;
import Modelo.ClasesJPA.Subgrupo;

/**
 *
 * @author Eric
 */
public class ComboBoxItem {
    private final Object objeto;
    
    public ComboBoxItem( Object o ){
        this.objeto = o;
    }   
    
    @Override
    public String toString(){
        if ( this.objeto.getClass() == Grupo.class ) {
            return ( (Grupo) this.objeto ).getNombre();
        }
        else if ( this.objeto.getClass() == Subgrupo.class ) {
            return ( (Subgrupo) this.objeto ).getNombre();
        }
        else if ( this.objeto.getClass() == Marca.class) {
            return ( (Marca) this.objeto ).getNombre();
        }
        else if ( this.objeto.getClass() == CentroCostos.class){
            CentroCostos cc = (CentroCostos) this.objeto;
            return cc.getNombre() + " - " + cc.getCodigo();
        } 
        else {
            return this.objeto.toString();
        }
    }

    public Object getObjeto() {
        return objeto;
    }

}
