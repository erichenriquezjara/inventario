package Utilidades;

import Configuracion.ParametrosGenerales;
import Configuracion.VentanasGenerales;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class GenerarReportes {

    public static void crearReporteSalidaStock(HashMap map, JFrame padre, String reporte) throws JRException {
        Connection conexion = null;
        try {
            conexion = DriverManager.getConnection(ParametrosGenerales.getStringConexion(), ParametrosGenerales.getUsuarioConexion(), ParametrosGenerales.getContraseñaConexion()
            );
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        BufferedImage image = null;
        try {
            image = ImageIO.read(GenerarReportes.class.getResource("/img/serfonac_letras_laterales.png"));
        } catch (IOException ex) {
            Logger.getLogger(GenerarReportes.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (image != null) {
            map.put("URL_LOGO", image);
        }
        
        InputStream subReporteJasper = GenerarReportes.class.getResourceAsStream("/reportes/FirmaTaco.jasper" );
        if( subReporteJasper != null ){
            map.put("SUBREPORTE", subReporteJasper);
            System.out.println( subReporteJasper.toString() );
        }        

        if (ParametrosGenerales.getUsuario() != null) {
            map.put("USUARIO_SISTEMA", ParametrosGenerales.getUsuario().getNombre());
        }
        
        String reporteJasper = "/reportes/Salidas.jasper";
        
        if( reporte.equals("PEQUEÑO") ){
            reporteJasper = "/reportes/SalidasTM.jasper";
        }

        InputStream rutaJasper = GenerarReportes.class.getResourceAsStream( reporteJasper );
        JasperPrint jasperPrint = JasperFillManager.fillReport(rutaJasper, map, conexion);
        VentanasGenerales.jasperViewer = new JasperViewer(jasperPrint, false);
        JDialog dialog = new JDialog(padre);
        dialog.setContentPane(VentanasGenerales.jasperViewer.getContentPane());
        dialog.setSize(VentanasGenerales.jasperViewer.getSize());
        dialog.setTitle("Salida de Stock");
        dialog.setVisible(true);
    }
    
    public static void crearReporteSalidaStockTermal(HashMap map, JFrame padre) throws JRException {
        Connection conexion = null;
        try {
            conexion = DriverManager.getConnection(ParametrosGenerales.getStringConexion(), ParametrosGenerales.getUsuarioConexion(), ParametrosGenerales.getContraseñaConexion()
            );
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        BufferedImage image = null;
        try {
            image = ImageIO.read(GenerarReportes.class.getResource("/img/serfonac_letras_laterales.png"));
        } catch (IOException ex) {
            Logger.getLogger(GenerarReportes.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (image != null) {
            map.put("URL_LOGO", image);
        }

        if (ParametrosGenerales.getUsuario() != null) {
            map.put("USUARIO_SISTEMA", ParametrosGenerales.getUsuario().getNombre());
        }

        InputStream rutaJasper = GenerarReportes.class.getResourceAsStream("/reportes/Salidas.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(rutaJasper, map, conexion);
        VentanasGenerales.jasperViewer = new JasperViewer(jasperPrint, false);
        JDialog dialog = new JDialog(padre);
        dialog.setContentPane(VentanasGenerales.jasperViewer.getContentPane());
        dialog.setSize(VentanasGenerales.jasperViewer.getSize());
        dialog.setTitle("Salida de Stock");
        dialog.setVisible(true);
    }    

    public static void crearReporteGenerarCodigosBarraPorGrupo(HashMap map, JFrame padre) throws JRException {
        Connection conexion = null;
        try {
            conexion = DriverManager.getConnection(ParametrosGenerales.getStringConexion(), ParametrosGenerales.getUsuarioConexion(), ParametrosGenerales.getContraseñaConexion()
            );
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        BufferedImage image = null;
        try {
            image = ImageIO.read(GenerarReportes.class.getResource("/img/serfonac_logo.png"));
        } catch (IOException ex) {
            Logger.getLogger(GenerarReportes.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (image != null) {
            map.put("URL_LOGO", image);
        }

        InputStream rutaJasper = GenerarReportes.class.getResourceAsStream("/reportes/GenerarCodigoBarrasPorGrupo.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(rutaJasper, map, conexion);
        VentanasGenerales.jasperViewer = new JasperViewer(jasperPrint, false);
        JDialog dialog = new JDialog(padre);
        dialog.setContentPane(VentanasGenerales.jasperViewer.getContentPane());
        dialog.setSize(VentanasGenerales.jasperViewer.getSize());
        dialog.setTitle("Generar Códigos de Barra");
        dialog.setVisible(true);
    }
}
