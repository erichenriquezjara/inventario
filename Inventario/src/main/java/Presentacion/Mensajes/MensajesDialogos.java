/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion.Mensajes;

import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;

/**
 *
 * @author Eric
 */
public class MensajesDialogos {
    
    public static void nadaSeleccionado( Component rootPane ){
        JOptionPane.showMessageDialog( rootPane, "No ha seleccionado ningún registro", "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public static void devolverMasDeLoSolicitado( Component rootPane ){
        JOptionPane.showMessageDialog( rootPane, "No es posible devolver más artículos de los solicitados.", "Error", JOptionPane.ERROR_MESSAGE);
    } 
    
    public static void solicitarMasDeLoExistente( Component rootPane ){
        JOptionPane.showMessageDialog( rootPane, "No es posible hacer una solicitud de más artículos de los que hay en stock.", "Error", JOptionPane.ERROR_MESSAGE);
    }    
    
    public static void cantidadIgualMenorCero( Component rootPane ){
        JOptionPane.showMessageDialog( rootPane, "La cantidad debe ser mayor a 0", "Error", JOptionPane.ERROR_MESSAGE);
    }    
    
    public static void numeroDocumentoVacio( Component rootPane ){
        JOptionPane.showMessageDialog( rootPane, "El Número de Documento no puede estar vacío", "Error", JOptionPane.ERROR_MESSAGE);
    }    
    
    public static void inicioDeSesionIncorrecto( Component rootPane ){
        JOptionPane.showMessageDialog( rootPane, "No se ha podido iniciar sesión con esas credenciales", "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public static int confirmaEliminacion( Component rootPane ){
        return JOptionPane.showConfirmDialog( rootPane , "¿Está seguro de querer eliminar este registro?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    }

    public static void noPuedeEliminarSubgrupo(JRootPane rootPane) {
        JOptionPane.showMessageDialog( rootPane, "No ha puede eliminar un grupo que tiene SubGrupos", "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void noPuedeEliminarProveedor(JRootPane rootPane) {
        JOptionPane.showMessageDialog( rootPane, "No ha puede eliminar este proveedor, tiene solicitudes asignadas", "Error", JOptionPane.ERROR_MESSAGE);
    }    

    public static void noExisteMarcaAsociada( Component rootPane) {
        JOptionPane.showMessageDialog( rootPane, "No existe una marca asociada, es necesario crearla en el Administrador de Marcas", "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void tablaVacia(JRootPane rootPane) {
        JOptionPane.showMessageDialog( rootPane, "La tabla está vacía, debe agregar registros", "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public static void quienEntregaVacio(JRootPane rootPane) { 
        JOptionPane.showMessageDialog(rootPane, "El campo \"Quién Entrega\" no puede estar vacío", "Error", JOptionPane.ERROR_MESSAGE); 
    }

    public static void quienRecibeVacio(JRootPane rootPane) { 
        JOptionPane.showMessageDialog(rootPane, "El campo \"Quién Recibe\" no puede estar vacío", "Error", JOptionPane.ERROR_MESSAGE); 
    }

    public static void solicitanteVacio(JRootPane rootPane) {
        JOptionPane.showMessageDialog( rootPane, "El campo Solicitante no puede estar vacío", "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void solicitudEntradaCorrecta( JRootPane rootPane ) {
        JOptionPane.showMessageDialog( rootPane, "Ingreso de stock agregado correctamente", "Información", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void solicitudSalidaCorrecta( JRootPane rootPane ) {
        JOptionPane.showMessageDialog( rootPane, "Salida de stock registrada correctamente", "Información", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void solicitudDevolucionCorrecta( JRootPane rootPane ) {
        JOptionPane.showMessageDialog( rootPane, "Devolución de stock registrada correctamente", "Información", JOptionPane.INFORMATION_MESSAGE);
    }    

    public static void errorCrearCodigo( JRootPane rootPane ) {
        JOptionPane.showMessageDialog( rootPane, "Ocurrió un error al generar el (los) código(s) de barras", "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void archivoGeneradoCorrectamente(JRootPane rootPane) {
        JOptionPane.showMessageDialog( rootPane, "El archivo se generó correctamente.", "Información", JOptionPane.INFORMATION_MESSAGE);
    }
}
