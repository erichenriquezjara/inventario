/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Utilidades.Listeners.ListenerTablaDetalleArticulos;
import Configuracion.ParametrosGenerales;
import Configuracion.VentanasGenerales;
import Modelo.ClasesJPA.Solicitud;
import Modelo.ControladorJPA;
import Modelo.SistemaStockQuerys;
import Presentacion.Mensajes.MensajesDialogos;
import Utilidades.GenerarReportes;
import Utilidades.InventarioTableModel;
import Utilidades.Listeners.ListenerJDayChooser;
import Utilidades.Listeners.ListenerTablaBusquedaSolicitudes;
import Utilidades.UtilidadesJTable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Eric
 */
public class BusquedaSolicitudes extends javax.swing.JFrame {

    private String tipo = "SALIDAS";
    public Integer fila_seleccionada = -1;    

    /**
     * Creates new form BusquedaSolicitudes
     */
    public BusquedaSolicitudes() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpRadio = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSolicitudes = new javax.swing.JTable();
        radioEntradas = new javax.swing.JRadioButton();
        radioSalidas = new javax.swing.JRadioButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblArticulos = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        dateInicio = new com.toedter.calendar.JDateChooser();
        dateFin = new com.toedter.calendar.JDateChooser();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtBusqueda = new javax.swing.JTextField();
        btnVerReporte = new javax.swing.JButton();
        btnVerReportePequeño = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lblTitulo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("Búsqueda Solicitudes");
        lblTitulo.setOpaque(true);

        tblSolicitudes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Solicitante", "Comentario", "N° Documento"
            }
        ));
        jScrollPane1.setViewportView(tblSolicitudes);

        grpRadio.add(radioEntradas);
        radioEntradas.setText("ENTRADAS");
        radioEntradas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioEntradasActionPerformed(evt);
            }
        });

        grpRadio.add(radioSalidas);
        radioSalidas.setSelected(true);
        radioSalidas.setText("SALIDAS");
        radioSalidas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        radioSalidas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioSalidasActionPerformed(evt);
            }
        });

        tblArticulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Solicitante", "Comentario", "N° Documento"
            }
        ));
        jScrollPane2.setViewportView(tblArticulos);

        jLabel3.setText("Detalle");

        jLabel1.setText("Desde");

        jLabel2.setText("Hasta");

        jLabel4.setText("Por Fecha:");

        dateInicio.setDateFormatString("dd/MM/yyyy");

        dateFin.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateInicio, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateFin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dateInicio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dateFin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(71, 71, 71))
        );

        jLabel5.setText("Artículo");

        jLabel7.setText("Por Artículo:");

        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        btnVerReporte.setText("Abrir Reporte");
        btnVerReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerReporteActionPerformed(evt);
            }
        });

        btnVerReportePequeño.setText("Abrir Reporte Pequeño");
        btnVerReportePequeño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerReportePequeñoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(368, 368, 368)
                                .addComponent(radioSalidas, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioEntradas))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnVerReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnVerReportePequeño, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioSalidas)
                    .addComponent(radioEntradas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVerReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVerReportePequeño, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(107, 107, 107))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radioSalidasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioSalidasActionPerformed
        this.tipo = this.radioSalidas.getText();
        this.btnVerReporte.setEnabled(true);
        this.btnVerReportePequeño.setEnabled(true);
        iniciarTablaSolicitudes("POR_FECHA");
    }//GEN-LAST:event_radioSalidasActionPerformed

    private void radioEntradasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioEntradasActionPerformed
        this.tipo = this.radioEntradas.getText();
        this.btnVerReporte.setEnabled(false);
        this.btnVerReportePequeño.setEnabled(false);
        iniciarTablaSolicitudes("POR_FECHA");
    }//GEN-LAST:event_radioEntradasActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        VentanasGenerales.menu.configuracionBasica();
        setVisible(false);
    }//GEN-LAST:event_formWindowClosing

    private void txtBusquedaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyPressed
        if (10 == evt.getKeyCode()) {
            iniciarTablaSolicitudes("POR_TEXTO");
        }
    }//GEN-LAST:event_txtBusquedaKeyPressed

    private void btnVerReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerReporteActionPerformed
        int selectedRow = this.tblSolicitudes.getSelectedRow();
        if (selectedRow != -1) {
            Integer idSolicitud = Integer.valueOf(this.tblSolicitudes.getValueAt(selectedRow, 0).toString());
            ControladorJPA.iniciarBD();
            Solicitud sol = (Solicitud) ControladorJPA.em.find(Solicitud.class, idSolicitud);
            HashMap map = new HashMap();
            map.put("ID", sol.getId());
            map.put("CENTRO_DE_COSTO", sol.getCentroCostos().getNombre() + " - " + sol.getCentroCostos().getCodigo());
            map.put("COMENTARIO", sol.getComentario());
            map.put("FECHA_SOLICITUD", sol.getFecha());
            map.put("NUMERO_DOCUMENTO", sol.getId().toString());
            map.put("QUIEN_ENTREGA", sol.getUsuario().getNombre());
            map.put("QUIEN_SOLICITA", sol.getSolicitante());            
            try {
                GenerarReportes.crearReporteSalidaStock(map, this, "GRANDE");
            } catch (Exception e) {
                System.out.println(" --- ERROR EN EL REPORTE --- ");
                e.printStackTrace();
            }
        } else {
            MensajesDialogos.nadaSeleccionado(this.rootPane);
        }
    }//GEN-LAST:event_btnVerReporteActionPerformed

    private void btnVerReportePequeñoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerReportePequeñoActionPerformed
        int selectedRow = this.tblSolicitudes.getSelectedRow();
        if (selectedRow != -1) {
            Integer idSolicitud = Integer.valueOf(this.tblSolicitudes.getValueAt(selectedRow, 0).toString());
            ControladorJPA.iniciarBD();
            Solicitud sol = (Solicitud) ControladorJPA.em.find(Solicitud.class, idSolicitud);
            HashMap map = new HashMap();
            map.put("ID", sol.getId());
            map.put("CENTRO_DE_COSTO", sol.getCentroCostos().getNombre() + " - " + sol.getCentroCostos().getCodigo());
            map.put("COMENTARIO", sol.getComentario());
            map.put("FECHA_SOLICITUD", sol.getFecha());
            map.put("NUMERO_DOCUMENTO", sol.getId().toString());
            map.put("QUIEN_ENTREGA", sol.getUsuario().getNombre());
            map.put("QUIEN_SOLICITA", sol.getSolicitante());            
            try {
                GenerarReportes.crearReporteSalidaStock(map, this, "PEQUEÑO");
            } catch (Exception e) {
                System.out.println(" --- ERROR EN EL REPORTE --- ");
                e.printStackTrace();
            }
        } else {
            MensajesDialogos.nadaSeleccionado(this.rootPane);
        }
    }//GEN-LAST:event_btnVerReportePequeñoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BusquedaSolicitudes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BusquedaSolicitudes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BusquedaSolicitudes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BusquedaSolicitudes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                BusquedaSolicitudes bs = new BusquedaSolicitudes();
                bs.configuracionBasica();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVerReporte;
    private javax.swing.JButton btnVerReportePequeño;
    private com.toedter.calendar.JDateChooser dateFin;
    private com.toedter.calendar.JDateChooser dateInicio;
    private javax.swing.ButtonGroup grpRadio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JRadioButton radioEntradas;
    private javax.swing.JRadioButton radioSalidas;
    private javax.swing.JTable tblArticulos;
    private javax.swing.JTable tblSolicitudes;
    private javax.swing.JTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables

    public void configuracionBasica() {
        setTitle(ParametrosGenerales.TITULO_BUSQUEDA_SOLICITUDES);
        setLocationRelativeTo(null);
        setVisible(true);

        // Tabla Solicitudes
        this.tblSolicitudes.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
        ListenerTablaBusquedaSolicitudes tblSolicitudesListener = new ListenerTablaBusquedaSolicitudes(this);
        this.tblSolicitudes.getSelectionModel().addListSelectionListener(tblSolicitudesListener);

        // Tabla Detalle (Artículos)
        this.tblArticulos.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
        ListenerTablaDetalleArticulos tblDetalleArticulosListener = new ListenerTablaDetalleArticulos(this);
        this.tblArticulos.getSelectionModel().addListSelectionListener( tblDetalleArticulosListener );

        ListenerJDayChooser listenerDayChooser = new ListenerJDayChooser(this);
        this.dateInicio.addPropertyChangeListener(listenerDayChooser);
        this.dateFin.addPropertyChangeListener(listenerDayChooser);
    }

    public void iniciarTablaSolicitudes(String opcion) {
        Object columnas[], data[][];

        Date ini = this.dateInicio.getDate();
        Date fin = this.dateFin.getDate();
        
        // Sólo continúa si ambas fechas son diferentes de null.
        if ( ini == null || fin == null ) {
            return;
        }
        
        ControladorJPA.iniciarBD();

        ArrayList<Solicitud> lista = SistemaStockQuerys.listaSolicitudPorTipoTexto(this.tipo, this.txtBusqueda.getText().trim().toUpperCase(), ini, fin);

        if (this.radioSalidas.isSelected()) {

            data = new Object[lista.size()][6];
            columnas = new Object[6];
            columnas[0] = "id";
            columnas[1] = "FECHA";
            columnas[2] = "SOLICITA";
            columnas[3] = "CENTRO DE COSTOS";
            columnas[4] = "COMENTARIO";
            columnas[5] = "N° DOC";

            for (int i = 0; i < lista.size(); i++) {
                Solicitud aux = (Solicitud) lista.get(i);
                data[i][0] = aux.getId();
                data[i][1] = (new SimpleDateFormat("dd/MM/yyyy")).format(aux.getFecha());
                data[i][2] = aux.getSolicitante();
                data[i][3] = aux.getCentroCostos().getNombre();
                data[i][4] = aux.getComentario();
                data[i][5] = aux.getNumeroDocumento();
            }

        } else {

            data = new Object[lista.size()][7];
            columnas = new Object[7];
            columnas[0] = "id";
            columnas[1] = "FECHA";
            columnas[2] = "PROVEEDOR";
            columnas[3] = "ENTREGA";
            columnas[4] = "RECIBE";
            columnas[5] = "COMENTARIO";
            columnas[6] = "N° DOC";

            for (int i = 0; i < lista.size(); i++) {
                Solicitud aux = (Solicitud) lista.get(i);
                data[i][0] = aux.getId();
                data[i][1] = (new SimpleDateFormat("dd/MM/yyyy")).format(aux.getFecha());
                data[i][2] = aux.getCentroCostos().getNombre();
                data[i][3] = aux.getSolicitante();
                data[i][4] = aux.getUsuario().getNombre();
                data[i][5] = aux.getComentario();
                data[i][6] = aux.getNumeroDocumento();
            }
        }

        InventarioTableModel dtm = new InventarioTableModel(data, columnas);
        this.tblSolicitudes.setModel(dtm);
        this.tblSolicitudes.setSelectionMode(0);
        UtilidadesJTable.esconderColumnaTabla(this.tblSolicitudes, 0);
        UtilidadesJTable.evitarMoverColumnas(this.tblSolicitudes);
    }

    public JTable getTblSolicitudes() {
        return tblSolicitudes;
    }

    public void buscarDetalle() {
        ControladorJPA.iniciarBD();
        int selectedRow = this.tblSolicitudes.getSelectedRow();
        Integer idSolicitud = Integer.valueOf(this.tblSolicitudes.getValueAt(selectedRow, 0).toString());
        ArrayList<Object[]> lista = SistemaStockQuerys.listaArticulosPorSolicitud(idSolicitud);

        System.out.println( lista );
        
        Object[][] data = new Object[lista.size()][6];
        Object[] columnas = new Object[6];
        columnas[0] = "Código Artículo";
        columnas[1] = "Grupo";
        columnas[2] = "Subgrupo";
        columnas[3] = "Marca";
        columnas[4] = "Artículo";
        columnas[5] = "Cantidad";

        for (int i = 0; i < lista.size(); i++) {
          data[i] = (Object[])lista.get(i);
        }

        InventarioTableModel dtm = new InventarioTableModel(data, columnas);
        this.tblArticulos.setModel(dtm);
        this.tblArticulos.setSelectionMode(0);
        UtilidadesJTable.esconderColumnaTabla(this.tblArticulos, 0);
        UtilidadesJTable.evitarMoverColumnas(this.tblArticulos);
    }
}
