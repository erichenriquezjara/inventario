/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Configuracion;

import Modelo.ClasesJPA.Usuario;
import Modelo.ControladorJPA;

/**
 *
 * @author Eric
 */
public class ParametrosGenerales {    
    // Test
    public static boolean test = false;
    
    // Identificador de la unidad de persistencia.
    public static final String PERSISTENCE_UNIT_NAME = "Inventario";
    
    // Login
    public static final String TITULO_LOGIN = "Stock Bodega - Serfonac";
    
    // Mantenedor de Grupos.
    public static final String TITULO_MANTENEDOR_GRUPOS = "Mantenedor de Grupos";
    
    // Mantenedor de SubGrupos
    public static final String TITULO_MANTENEDOR_SUBGRUPOS = "Mantenedor de Subgrupos";
    
    // Mantenedor de Artículos
    public static final String TITULO_MANTENEDOR_ARTICULOS = "Mantenedor de Artículos"; 
    
    // Mantenedor de Marcas
    public static final String TITULO_MANTENEDOR_MARCAS = "Mantenedor de Marcas";
    
    // Mantenedor Proveedores
    public static final String TITULO_MANTENEDOR_PROVEEDORES  = "Mantenedor Proveedores";
    
    // Entrada Stock
    public static final String TITULO_ENTRADA_STOCK = "Entrada de Stock";
    
    // Salida Stock
    public static final String TITULO_SALIDA_STOCK = "Salida de Stock";
    
    // Consulta Stock
    public static final String TITULO_CONSULTA_ARTICULO = "Consulta Stock";
    
    // Búsqueda Solicitudes
    public static String TITULO_BUSQUEDA_SOLICITUDES = "Búsqueda Solicitudes";
    
    // Búsqueda Artículos
    public static String TITULO_BUSQUEDA_ARTICULOS = "Búsqueda Artículos";
    
    // Devolución Stock
    public static String TITULO_DEVOLUCION_STOCK = "Devolución de Stock";
    
    // Usuario
    public static Usuario usuario = null;    

    
    public static Usuario getUsuario(){
        if ( ParametrosGenerales.usuario == null ) {
            return ControladorJPA.em.find( Usuario.class, 1);
        }
        else{
            return ParametrosGenerales.usuario;
        }
    }
    
    public static String getStringConexion(){
        if ( ParametrosGenerales.test ) {
            return "jdbc:mysql://127.0.0.1:3306/serfona1_stock?zeroDateTimeBehavior=convertToNull";
        }
        else{
            return "jdbc:mysql://serfonac.cl:3306/serfona1_stock?zeroDateTimeBehavior=convertToNull";
        }
    }
    
    public static String getUsuarioConexion(){
        if ( ParametrosGenerales.test ) {
            return "root";
        }
        else{
            return "serfona1_stockR";
        }
    }
    
    public static String getContraseñaConexion(){
        if ( ParametrosGenerales.test ) {
            return "";
        }
        else{
            return "stock123";
        }
    }
}
