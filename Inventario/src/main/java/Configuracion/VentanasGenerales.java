/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Configuracion;

import Presentacion.BusquedaArticulos;
import Presentacion.EntradaStock;
import Presentacion.BusquedaArticulosPorGrupo;
import Presentacion.BusquedaSolicitudes;
import Presentacion.Login;
import Presentacion.MantenedorArticulo;
import Presentacion.MantenedorGrupo;
import Presentacion.MantenedorMarca;
import Presentacion.MantenedorCentroCosto;
import Presentacion.MantenedorSubGrupo;
import Presentacion.Menu;
import Presentacion.DevolucionStock;
import Presentacion.SalidaStock;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Eric
 */
public class VentanasGenerales {
    public static Login login;
    public static Menu menu;
    public static MantenedorGrupo mantenedorGrupo;
    public static MantenedorSubGrupo mantenedorSubGrupo;    
    public static MantenedorMarca mantenedorMarca;
    public static MantenedorArticulo mantenedorArticulo;
    public static MantenedorCentroCosto mantenedorProveedor;    
    public static EntradaStock entradaStock;
    public static SalidaStock salidaStock;
    public static JasperViewer jasperViewer;
    public static BusquedaArticulosPorGrupo busquedaArticulosPorGrupo;
    public static BusquedaArticulos busquedaArticulos;
    public static BusquedaSolicitudes busquedaSolicitudes;
    public static DevolucionStock devolucionStock;
}
