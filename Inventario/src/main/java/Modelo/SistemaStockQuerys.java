/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.ClasesJPA.Articulo;
import Modelo.ClasesJPA.Grupo;
import Modelo.ClasesJPA.Marca;
import Modelo.ClasesJPA.CentroCostos;
import Modelo.ClasesJPA.Solicitud;
import Modelo.ClasesJPA.Subgrupo;
import Modelo.ClasesJPA.Usuario;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Query;

/**
 *
 * @author Eric
 */
public class SistemaStockQuerys {

    public static ArrayList<Grupo> listaGrupos() {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT g FROM Grupo g";
        Query query = ControladorJPA.em.createQuery(SQL);
        ArrayList<Grupo> lista = new ArrayList<Grupo>(query.getResultList());
        return lista;
    }

    public static ArrayList<Subgrupo> listaSubGrupos() {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT s FROM Subgrupo s";
        Query query = ControladorJPA.em.createQuery(SQL);
        ArrayList<Subgrupo> lista = new ArrayList<Subgrupo>(query.getResultList());
        return lista;
    }

    public static ArrayList<Subgrupo> listaSubGrupos(int idGrupo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT s FROM Subgrupo s Where s.grupo.id = :grupo";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("grupo", idGrupo);
        ArrayList<Subgrupo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Usuario> Login(String usuario, String clave) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT u FROM Usuario u Where u.usuario = :usuario AND u.clave = :clave";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("usuario", usuario);
        query.setParameter("clave", clave);
        ArrayList<Usuario> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Articulo> listaArticulos(int idMarca) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT a FROM Articulo a Where a.marca.id = :marca";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("marca", idMarca);
        ArrayList<Articulo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Articulo> listaArticulos() {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT a FROM Articulo a";
        Query query = ControladorJPA.em.createQuery(SQL);
        ArrayList<Articulo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Articulo> listaArticulosPorTexto(String texto) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT a FROM Articulo a Where a.descripcion LIKE :texto";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("texto", "%" + texto + "%");
        ArrayList<Articulo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Articulo> listaArticulosPorCodigo(String codigo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT a FROM Articulo a Where a.codigoArticulo = :codigo";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("codigo", codigo);
        ArrayList<Articulo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Object[]> listaArticulosDetallePorGrupo(int idGrupo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT\n"
                + "     articulo.codigo_articulo AS Código,\n"
                + "     grupo.nombre AS Grupo,\n"
                + "     subgrupo.nombre AS Subgrupo,\n"
                + "     marca.nombre AS Marca,\n"
                + "     articulo.descripcion AS Descripción,\n"
                + "     articulo.cantidad_disponible As Cantidad\n"
                + "FROM\n"
                + "     marca marca INNER JOIN articulo articulo ON marca.id = articulo.marca\n"
                + "     INNER JOIN subgrupo subgrupo ON marca.subgrupo = subgrupo.id\n"
                + "     INNER JOIN grupo grupo ON subgrupo.grupo = grupo.id\n"
                + "WHERE\n"
                + "     grupo.id = :idGrupo     \n"
                + "ORDER BY\n"
                + "	 grupo.nombre ASC, subgrupo.nombre ASC, marca.nombre ASC, articulo.descripcion ASC";
        Query query = ControladorJPA.em.createNativeQuery(SQL);
        query.setParameter("idGrupo", idGrupo);
        ArrayList<Object[]> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Marca> listaMarcas(int idSubGrupo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT m FROM Marca m Where m.subgrupo.id = :subgrupo";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("subgrupo", idSubGrupo);
        ArrayList<Marca> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<CentroCostos> listaCentroCostos(String tipo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT c FROM CentroCostos c Where c.tipo = :tipo order by nombre";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("tipo", tipo);
        ArrayList<CentroCostos> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static Date dateAhora() {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT NOW() FROM DUAL";
        Query query = ControladorJPA.em.createNativeQuery(SQL);
        ArrayList<Timestamp> lista = new ArrayList<>(query.getResultList());
        Calendar fechaAux = Calendar.getInstance();
        fechaAux.setTimeInMillis(((Timestamp) lista.get(0)).getTime());
        return fechaAux.getTime();
    }

    public static ArrayList<Articulo> listaArticulosPorTextoSalida(String texto) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT a FROM Articulo a Where a.descripcion LIKE :texto AND a.cantidadDisponible > 0";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("texto", "%" + texto + "%");
        ArrayList<Articulo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Articulo> listaArticulosPorCodigoSalida(String codigo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT a FROM Articulo a Where a.codigoArticulo = :codigo  AND a.cantidadDisponible > 0";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("codigo", codigo);
        ArrayList<Articulo> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<String> listaSolicitantes() {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT DISTINCT UPPER(s.solicitante) FROM solicitud s";
        Query query = ControladorJPA.em.createNativeQuery(SQL);
        return (ArrayList<String>) query.getResultList();
    }

    public static ArrayList<Solicitud> listaSolicitudFechaTipo(Date ini, Date fin, String tipo) {
        ControladorJPA.iniciarBD();
        String SQL = "SELECT s FROM Solicitud s WHERE s.fecha BETWEEN :ini AND :fin AND s.tipo = :tipo";
        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("ini", ini);
        query.setParameter("fin", fin);
        query.setParameter("tipo", tipo);
        ArrayList<Solicitud> lista = new ArrayList<>(query.getResultList());
        return lista;
    }

    public static ArrayList<Solicitud> listaSolicitudPorTipoTexto(String tipo, String texto, Date ini, Date fin) {
        ControladorJPA.iniciarBD();        
        
        Calendar calendarioAux = Calendar.getInstance();
        
        calendarioAux.setTime(ini);
        calendarioAux.set(Calendar.HOUR_OF_DAY, 0);
        calendarioAux.set(Calendar.MINUTE, 0);
        calendarioAux.set(Calendar.SECOND, 0);
        ini = calendarioAux.getTime();
        
        calendarioAux.setTime(fin);
        calendarioAux.set(Calendar.HOUR_OF_DAY, 23);
        calendarioAux.set(Calendar.MINUTE, 59);
        calendarioAux.set(Calendar.SECOND, 59);
        fin = calendarioAux.getTime();
        
        String SQL = "SELECT distinct s "
                     + "FROM SolArticulo sa "
                            + "INNER JOIN sa.idSolicitud s "
                            + "INNER JOIN sa.idArticulo a "
                    + "WHERE s.tipo = :tipo "
                            + "AND UPPER(a.descripcion) "
                            + "LIKE :texto "
                            + "AND s.fecha BETWEEN :ini "
                            + "AND :fin";

        Query query = ControladorJPA.em.createQuery(SQL);
        query.setParameter("tipo", tipo);
        query.setParameter("texto", "%" + texto.toUpperCase().trim() + "%");
        query.setParameter("ini", ini);
        query.setParameter("fin", fin);

        return new ArrayList<>(query.getResultList());
    }

    public static ArrayList<Object[]> listaArticulosPorSolicitud(Integer idSolicitud) {
        ControladorJPA.iniciarBD();
        String SQL = ""
                + "SELECT \n" +
                "    a.codigo_articulo AS CODIGO, \n" +
                "    g.nombre AS GRUPO,\n" +
                "    sg.nombre AS SUBGRUPO,\n" +
                "    m.nombre AS MARCA,\n" +
                "    a.descripcion AS ARTICULO,\n" +
                "    sa.cantidad AS CANTIDAD,\n" +
                "    sa.id AS ID_SOL_ART,\n" +
                "    a.id AS ID_ARTICULO \n" +
                "FROM \n" +
                    "solicitud s INNER JOIN \n" +
                    "sol_articulo sa ON sa.id_solicitud = s.id INNER JOIN \n" +
                    "articulo a ON sa.id_articulo = a.id INNER JOIN\n" +
                    "marca m ON m.id = a.marca INNER JOIN\n" +
                    "subgrupo sg ON sg.id = m.subgrupo INNER JOIN\n" +
                    "grupo g on sg.grupo = g.id\n" +
                "where s.id = :idSolicitud";
        Query query = ControladorJPA.em.createNativeQuery(SQL);
        query.setParameter("idSolicitud", idSolicitud);
        return new ArrayList<>(query.getResultList());
    }

    public static ArrayList<Object[]> ultimoPrecioUnitarioPorArticulo(Integer id) {
        ControladorJPA.iniciarBD();
        String SQL = ""
                + "  SELECT valor_unitario"
                + "    FROM sol_articulo "
                + "   WHERE id_articulo = :idArticulo "
                + "ORDER BY fecha_registro DESC LIMIT 1";
        Query query = ControladorJPA.em.createNativeQuery(SQL);
        query.setParameter("idArticulo", id);
        return new ArrayList<>( query.getResultList() );        
    }
    
    public static ArrayList<Object[]> centroDeCostosPorNombre(String nombre) {
        ControladorJPA.iniciarBD();
        String SQL = ""
                + "  SELECT ID"
                + "    FROM serfona1_stock.centro_costos "
                + "   WHERE UPPER(nombre) = UPPER(:nombreCentro) "
                + "   LIMIT 1";
        Query query = ControladorJPA.em.createNativeQuery(SQL);
        query.setParameter("nombreCentro", nombre);
        return new ArrayList<>( query.getResultList() );        
    }    

}
