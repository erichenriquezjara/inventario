/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Configuracion.ParametrosGenerales;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Eric
 */
public class ControladorJPA {
    public static EntityManagerFactory factory = null;
    public static EntityManager em = null;
    
    public static void iniciarBD(){
        
        if ( ControladorJPA.factory == null ) {
            System.out.println("... Intentando abrir conexión ...");
            ControladorJPA.factory = Persistence.createEntityManagerFactory( ParametrosGenerales.PERSISTENCE_UNIT_NAME );
            ControladorJPA.em = ControladorJPA.factory.createEntityManager();
            System.out.println("... Base de Datos abierta =D  ...");
        }
        else{
            try{
                String SQL = "SELECT 1";
                Query query = ControladorJPA.em.createNativeQuery(SQL);
                System.out.println(" -- PING");
                System.out.println( query.getResultList().get(0) );
            }
            catch(Exception ex){
                ControladorJPA.cerrarBD();
            
                System.out.println("... Intentando reconectar ...");
                ControladorJPA.factory = Persistence.createEntityManagerFactory( ParametrosGenerales.PERSISTENCE_UNIT_NAME );
                ControladorJPA.em = ControladorJPA.factory.createEntityManager();
                System.out.println("... Base de Datos abierta =D  ...");
            }
        }
    }
    
    public static void cerrarBD(){
        
        if ( ControladorJPA.factory.isOpen() ) {
            System.out.println("... Conexión abierta, intentando cerrar ...");
            ControladorJPA.factory.close();
            ControladorJPA.em.close();
            System.out.println("... Base de Datos cerrada ...");
        }
        
    }
}
