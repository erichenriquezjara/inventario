/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.ClasesJPA;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "centro_costos")
@NamedQueries({
    @NamedQuery(name = "CentroCostos.findAll", query = "SELECT c FROM CentroCostos c"),
    @NamedQuery(name = "CentroCostos.findById", query = "SELECT c FROM CentroCostos c WHERE c.id = :id"),
    @NamedQuery(name = "CentroCostos.findByNombre", query = "SELECT c FROM CentroCostos c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CentroCostos.findByCodigo", query = "SELECT c FROM CentroCostos c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "CentroCostos.findByComentario", query = "SELECT c FROM CentroCostos c WHERE c.comentario = :comentario")})
public class CentroCostos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "comentario")
    private String comentario;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "centroCostos")
    private List<Solicitud> solicitudList;

    public CentroCostos() {
    }

    public CentroCostos(Integer id) {
        this.id = id;
    }

    public CentroCostos(Integer id, String nombre, String codigo) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }    

    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CentroCostos)) {
            return false;
        }
        CentroCostos other = (CentroCostos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.ClasesJPA.CentroCostos[ id=" + id + " ]";
    }    
}
